<?php $ishome = 1; include_once "includes/header.php";?>     
    <div class="content-wrapper caseStudy_wrapper">
    
    	<div class="main-banner slick-instance" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "fade": true, "infinite": false, "arrows": true, "dots": true, "autoplay": true, "autoplaySpeed": 8000, "centerMode": false}'>
			
			<div class="image">
				<img src="src/images/main-banner-casestudy.png" alt="main-banner">
				<div class="container">
					<div class="caption">
						<h1>Case Study Title 1</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
					</div>
				</div>
			</div>
			
		</div>
		<div class="container">
			<div class="inner-content">
				<div class="desc-wrapper">
					<div>
						<h2>Improving Infrastructure Agility and Cost Efficiency on AWS</h2>
						<p>The fun-to-use app generates videos of users based on a source video uploaded by the user doing basic motions, like moving around, kicking their legs or waving their arms. Its PyTorch-backed proprietary GAN Neural Network model creates a digital skeleton of the user. Using that skeleton, users can generate a new, photo-realistic stunt double of themselves dancing like Michael Jackson, twirling like a ballet dancer, or making karate moves like a Black Belt in 30 seconds.</p>
						<p>The fun-to-use app generates videos of users based on a source video uploaded by the user doing basic motions, like moving around, kicking their legs or waving their arms. Its PyTorch-backed proprietary GAN Neural Network model creates a digital skeleton of the user. Using that skeleton, users can generate a new, photo-realistic stunt double of themselves dancing like Michael Jackson, twirling like a ballet dancer, or making karate moves like a Black Belt in 30 seconds.</p>
					</div>
					<div class="desc-box">
						<h4>Industry</h4>
						<p>SaaS, Media & Entertainment</p>
						<h4>Challenge</h4>
						<p>Develop a foundation for rapid application releases that will increase infrastructure cost-effectiveness and improve application efficiency and performance on AWS.</p>
						<h4>Services & Tech</h4>
						<p>Amazon ECS, Amazon ECR, Spot ASG (containers on ECS), AWS Step Functions, AWS Lambda (serverless)</p>
					</div>
				</div>
				<div class="main-image">
					<img src="src/images/casestudyMain.png" alt="">
				</div>
				<div class="desc-wrapper">
					<div>
						<h3>The Problem</h3>
						<p>The app’s operation relied on over 400 AWS G4 on-demand instances controlled by a complex and expensive-to-maintain system that was built internally. During the 2020 Super Bowl, the app was launched in partnership with Doritos after which it hit the number two position in the Apple App Store and, with help from AWS, Humen.Ai was able to scale up to handle the traffic spike.</p>
						<p>The successful launch uncovered scalability and efficiency issues in its backend. The AI app’s concept was built on its ability to quickly train models for each customer which required an immense amount of on-demand compute power as is common in any AI-driven application.</p>
						<p>“One thing that we were pretty concerned about was the cost of compute in the backend, ” recalled Tinghui Zhou, Co-founder and CEO of Humen.Ai. Our infrastructure is driven by ML which requires the usage of graphics processing units (GPUs) for model training and inference on the cloud.”</p>
					</div>
					<div class="desc-box">
						<h4>Highlight</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
					</div>
				</div>
				<div class="desc-wrapper">
					<div>
						<h3>The Solution</h3>
						<p>
							The Humen.Ai team was both extremely proficient in AI/ML operations and in AWS. They had tried a few pathways to optimize infrastructure, like containerization, but those didn’t work out. Amazon SageMaker would have been a great option for model training and inference as it lets you run on Spot Instances, a less expensive route compared to on-demand instances. The trade-off is the additional wait time due to AWS’ need to allocate capacity to run spare training which would hinder the end user experience. Users needed to quickly upload a video, have it immediately processed and be able to generate Instagram filters in minutes.  
						</p>
						<p>As part of the Jumpstart program, AWS referred Humen.Ai to Onica, a Rackspace Technology company for containerization support. The Jumpstart program provides organizations with low-cost infrastructure, credits and training to support growth. Working with Onica, they were able to get to the bottom of the containerization issues. The Onica team helped Humen.Ai containerize its entire AI app to effortlessly deploy on Amazon ECS with Spot Instances. </p>
					</div>
					<div class="desc-box">
						<h4>Highlight</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
					</div>
					
				</div>
				<div class="image-wrapper">
					<div class="row">
						<div class="col-md-4">
							<div class="image">
								<img src="src/images/solution-1.png" alt="">
							</div>
						</div>
						<div class="col-md-4">
							<div class="image">
								<img src="src/images/solution-2.png" alt="">
							</div>
						</div>
						<div class="col-md-4">
							<div class="image">
								<img src="src/images/solution-3.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="solid-bg">
			<div class="container">
				<div class="desc-wrapper">
					<div>
						<h3>The Outcome</h3>
						<p>
							perations and in AWS. They had tried a few pathways to optimize infrastructure, like containerization, but those didn’t work out. Amazon SageMaker would have been a great option for model training and inference as it lets you run on Spot Instances, a less expensive route compared to on-demand instances. The trade-off is the additional wait time due to AWS’ need to allocate capacity to run spare training which would hinder the end user experience. Users needed to quickly upload a video, have it immediately processed and be able to generate Instagram filters in minutes.   
						</p>
						<p>As part of the Jumpstart program, AWS referred Humen.Ai to Onica, a Rackspace Technology company for containerization support. The Jumpstart program provides organizations with low-cost infrastructure, credits and training to support growth. Working with Onica, they were able to get to the bottom of the containerization issues. The Onica team helped Humen.Ai containerize its entire AI app to effortlessly deploy on Amazon ECS with Spot Instances. </p>
					</div>
					<div class="desc-box">
						<h4>Highlight</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
					</div>
					
				</div>
			</div>
		</div>
    </div>
<?php include_once "includes/footer.php";?>