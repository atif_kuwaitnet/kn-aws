import { StyleProp, TextStyle } from 'react-native'

export const header-fixed: StyleProp<TextStyle>
export const header-wrapper: StyleProp<TextStyle>
export const left-side: StyleProp<TextStyle>
export const menu-icon: StyleProp<TextStyle>
export const logo: StyleProp<TextStyle>
export const right-side: StyleProp<TextStyle>
export const sticky: StyleProp<TextStyle>
