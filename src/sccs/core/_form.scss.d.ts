import { StyleProp, TextStyle } from 'react-native'

export const form-group: StyleProp<TextStyle>
export const telephone: StyleProp<TextStyle>
export const bootstrap-select: StyleProp<TextStyle>
export const form-control: StyleProp<TextStyle>
export const control-label: StyleProp<TextStyle>
export const active: StyleProp<TextStyle>
export const form-control:placeholder-shown+label: StyleProp<TextStyle>
export const form-control:focus+label: StyleProp<TextStyle>
export const form-control:-ms-input-placeholder+label: StyleProp<TextStyle>
export const form-control:-ms-input-placeholder: StyleProp<TextStyle>
