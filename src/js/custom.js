var chart;
$(document).ready(function() {
    olshi.init({
        slick: $(".slick-instance"),
        datepicker_inline: $("#datepicker_inline"),
        timepicker: $("#timepicker, #timepicker2, #timepicker3, #timepicker4"),
        datepicker: $("#datepicker"),
        datepicker2: $("#datepicker2"),
        galleryMoreImages: ".photo-gallery .upload-image input[type='file']"
    });
});

var self;
var olshi = {
    init: function(options) {
        this.settings = options;
        self = this;

        this.utilities();
        this.loader();
        this.progressbar();
        this.pricerangeSlider(); 
        this.rating();
        this.configureModal();
        this.zoom();
        this.pro_detail();
        this.stickyHeader();
        // this.loadmore();
        this.addMoreFiles();
        this.submenu();
        this.changeImage();
        this.datepickers();
        this.selectpicker();
        this.galleryMoreImages();
    },
    loader: function() {
        setTimeout(function() {
            $('body').addClass('loaded');
        }, 2500);
    },

    // datepickers

    datepickers: function() {
        this.settings.timepicker.datetimepicker({
            format: 'LT',
            ignoreReadonly: true,
            keepOpen: false,
        });
        this.settings.datepicker_inline.datetimepicker({
            inline: true,
            format: 'DD/MM/YYYY',
        });
        this.settings.datepicker.datetimepicker({
            format: 'L',
            keepOpen: false,
            ignoreReadonly: true,
        });
        this.settings.datepicker2.datetimepicker({
            format: 'L',
            keepOpen: false,
            ignoreReadonly: true,
        });
    },

    stickyHeader: function() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 180) {
                $(".header-fixed").addClass("sticky");

            } else {
                $(".header-fixed").removeClass("sticky");

            }
        });
    },

    // selectpicker
    selectpicker: function() {
        $.fn.selectpicker.Constructor.BootstrapVersion = '4';
        if (/Android|webOS|iPhone|BlackBerry/i.test(navigator.userAgent)) {
            $.fn.selectpicker.Constructor.DEFAULTS.mobile = true;
        }
        $('select').selectpicker({

            size: 8,
            liveSearchPlaceholder: 'Search'
        });
    },
    galleryMoreImages: function() {

        $(document).on("change", ".profile input[type='file']", function(e) {
            var crnt = $(this).parents(".upload-image").find(".profile");
            var files = $(this)[0].files;
            for (i = 0; i < files.length; i++) {
                var readImg = new FileReader();
                var file = files[i];

                if (file.type.match('image.*')) {
                    readImg.onload = (function(file) {
                        return function(e) {
                            $(crnt).find("img").remove();
                            $(crnt).append(
                                "<img src='" + e.target.result + "' alt=''>"
                            );
                        };

                    })(file);
                    readImg.readAsDataURL(file);
                }

            }
        });
    },
    submenu: function() {
        $(".has-submenu .sub-item").on("click", function(e) {
            e.preventDefault();
            $(this).next("ul.sub-menu").slideToggle();
        });

    
        // for desktop
        if ($(window).width() > 768) {
            // $(".has-sidemenu .sub-item span").on("hover", function(e) {
            //     e.preventDefault();
            //     $(this).parents('.has-sidemenu').toggleClass('active');
            // });

            // new
            $('.has-sidemenu').mouseover(function() {

                $(this).addClass('active');

            });
            $('.has-sidemenu').mouseleave(function() {

                $(this).removeClass('active');

            });
            // new
        }
        // for desktop

        if ($(window).width() < 767) {
            $(".has-sidemenu .sub-item").on("click", function(e) {
                e.preventDefault();
                $(this).next("ul").slideToggle();
            });
        }
        // $(".has-submenu.has-sidemenu .sub-item").on("click", function() {

        //     $(this).next("ul.sub-menu").stop();

        // });
        window.addEventListener('resize', function(e){


            
            if ($(window).width() > 768) {
                // $(".has-sidemenu .sub-item span").on("hover", function(e) {
                //     e.preventDefault();
                //     $(this).parents('.has-sidemenu').toggleClass('active');
                // });

                // new
                $('.has-sidemenu').mouseover(function() {

                    $(this).addClass('active');

                });
                $('.has-sidemenu').mouseleave(function() {

                    $(this).removeClass('active');

                });
                // new
            }
            

            if ($(window).width() < 767) {
                $(".has-sidemenu .sub-item").on("click", function(e) {
                    e.preventDefault();
                    $(this).next("ul").slideToggle();
                });
            }

        });

    },
    progressbar: function() {
        $('.progress').asProgress({
            'namespace': 'progress'
        });
        $(".product-details .review-rating").on("click", function(e) {
            e.preventDefault();
            $(".review-rating .rating-animation").fadeOut("fast");
            $(".review-rating .rating-animation").fadeIn("slow");
            setTimeout(function() {
                $('.progress').asProgress('start');
            }, 400);
        });
        $(".product-details .review-rating .dropdown").on("mouseover", function(e) {
            e.preventDefault();
            $(".review-rating .rating-animation").fadeOut("fast");
            $(".review-rating .rating-animation").fadeIn("slow");
            setTimeout(function() {
                $('.progress').asProgress('start');
            }, 400);
        });

    },
    rating: function() {
        $(".rating").starRating({
            totalStars: 5,
            emptyColor: '#c2c2c2',
            hoverColor: '#9b176e',
            activeColor: '#9b176e',
            ratedColor: '#9b176e',
            initialRating: 0,
            strokeWidth: 0,
            useGradient: false,
            minRating: 1,
            starSize: 20
        });
        $(".rating-fixed").starRating({
            totalStars: 5,
            emptyColor: '#c2c2c2',
            hoverColor: '#9b176e',
            activeColor: '#9b176e',
            initialRating: 4,
            strokeWidth: 0,
            useGradient: false,
            minRating: 1,
            readOnly: true,
            starSize: 20
        });

    },
    changeImage: function() {

        // $(document).on("change", ".upload-btn input[type='file']", function(e) {
        //     var crnt = $(this).parents(".upload-btn");
        //     var files = $(this)[0].files;
        //     for (i = 0; i < files.length; i++) {
        //         var readImg = new FileReader();
        //         var file = files[i];

        //         if (file.type.match('image.*')) {
        //             readImg.onload = (function(file) {
        //                 return function(e) {
        //                     $(crnt).append(
        //                         "<img src='" + e.target.result + "' alt=''>"
        //                     );
        //                 };
        //             })(file);
        //             readImg.readAsDataURL(file);
        //         }
        //     }
        // });
        $(document).on("change", ".upload-btn input[type='file']", function(e) {
            var crnt = $(this).parents(".upload-btn");
            var files = $(this)[0].files;
            for (i = 0; i < files.length; i++) {
                var readImg = new FileReader();
                var file = files[i];

                if (file.type.match('image.*')) {
                    readImg.onload = (function(file) {
                        return function(e) {
                            $(crnt).append(
                                "<img src='" + e.target.result + "' alt=''>"
                            );
                            $(crnt).addClass("image-added");

                        };
                    })(file);
                    readImg.readAsDataURL(file);
                }
            }
        });

    },
    addMoreFiles: function() {
        $(document).on("click", ".add-more", function(e) {
            e.preventDefault();
            var html = '<div class="upload-btn">' +
                '<input type="file" name="">' +
                '</div>';
            $(this).before(html);

        });
    },
    pricerangeSlider: function() {
        if ($("input").hasClass("range-slider")) {
            var mySlider = $("input.range-slider").slider();
            $("input.range-slider").on("slide", function(sliderValue) {
                val = sliderValue.value;
                $(".price-slider label").html("(" + val[0] + " KWD - " + val[1] + " KWD)");
            });
        }
    },
    // zoom: function () {
    //     if ($(window).width() > 1025) {
    //         $(".product-slider .main-slider").elevateZoom({
    //             zoomType: "inner",
    //             cursor: "crosshair",
    //             zoomWindowPosition: zwindowpost
    //         });
    //     }
    // },
    zoom: function() {
        var zwindowpost = 1;
        if ($("html").hasClass("ar")) {
            zwindowpost = 11;
        }
        if ($(window).width() > 1025) {
            $(".product-slider .main-slider").elevateZoom({
                // zoomType: "inner",
                cursor: "crosshair",
                zoomWindowPosition: zwindowpost,
                // zoomWindowPosition: 1,
                zoomWindowOffsetX: 10,
                zoomWindowHeight: 450,
                zoomWindowWidth: 450
            });

        }
    },
    pro_detail: function() {

        $(".product-slider .thumb-slider").on('afterChange', function(event, slick, currentSlide, nextSlide) {

            $(".product-slider .main-slider").removeData('zoom-image');
            $('.zoomContainer').remove();
            var newUrl = $(".product-slider .thumb-slider .slick-current .image").attr("data-zoom-image");
            $(".product-slider .main-slider").attr("data-zoom-image", newUrl);
            if (newUrl) {
                olshi.zoom();
            } else {
                $('.zoomContainer').remove();
            }
        });


    },
    // modal
    configureModal: function() {
        $("body").on("click", "*[data-toggle='custom-modal']", function(e) {
            e.preventDefault();
            $(".custom-modal").removeClass("large");
            var url = $(this).attr("data-path");
            var size = $(this).attr("data-size");
            var class_name = $(this).attr("data-class");
            $(".custom-modal").removeClass("large");
            $(".custom-modal").removeClass("medium");
            $(".custom-modal").removeClass("small");
            $.get(url, function(data) {
                $(".custom-modal").modal("show");
                $(".custom-modal .modal-body").html(data);

                if (size) {
                    $(".custom-modal").addClass(size);
                }
                if (class_name) {
                    $(".custom-modal").addClass(class_name);
                }
                setTimeout(function() {
                    $(".custom-modal .modal-body").addClass("show");
                }, 200);
                $("body").addClass("remove-scroll");
            });
        });
        $(".modal").on("hidden.bs.modal", function() {
            $(".custom-modal .modal-body").removeClass("show");
            $(".custom-modal .modal-body").empty();
            $(".custom-modal").removeClass("account-modal");
            $("body").removeClass("remove-scroll");
            $(".custom-modal").removeClass("large");
            $(".custom-modal").removeClass("medium");
            $(".custom-modal").removeClass("small");
        });
    },
    utilities: function() {
        var $slider = $('.main-banner');
        if ($slider.length) {
            $slider.on('afterChange', function(event, slick, currentSlide) {
                $slide = $slider.find(".slick-active");
                var type = $slide.find(".video").attr("data-type");
                var video = "";
                if (type) {
                    if (type == "youtube") {
                        video = $slide.find(".video").attr("data-video");
                        $slide.find("iframe").attr("src", video);
                        var videos = $slider.find("video");
                        $(videos).each(function() {
                            $(this)[0].pause();
                        })
                    } else if (type == "mp4") {
                        $slider.find("iframe").attr("src", "");
                        $slide.find("video")[0].play();
                    } else {
                        $slider.find("iframe").attr("src", video);
                        slider.find("video")[0].pause();
                    }
                } else {
                    $slider.find("iframe").attr("src", video);
                    var videos = $slider.find("video");
                    $(videos).each(function() {
                        $(this)[0].pause();
                    });
                }
            });
        }
        // AOS.init({
        //     delay: 100, 
        //     duration: 900, 
        // });

        $('.menu-close').on('click', function(e) {
            e.preventDefault();
            $('.mp-pusher').removeClass('mp-pushed');
        });

        var $slider = $('.product-slider .main-slider');
        if ($slider.length) {
            $slider.on('afterChange', function(event, slick, currentSlide) {
                $slide = $slider.find(".slick-active");
                var type = $slide.find(".video").attr("data-type");
                var video = "";
                if (type) {
                    if (type == "youtube") {
                        video = $slide.find(".video").attr("data-video");
                        $slide.find("iframe").attr("src", video);
                        var videos = $slider.find("video");
                        $(videos).each(function() {
                            $(this)[0].pause();
                        })
                    } else if (type == "mp4") {
                        $slider.find("iframe").attr("src", "");
                        $slide.find("video")[0].play();
                    } else {
                        $slider.find("iframe").attr("src", video);
                        slider.find("video")[0].pause();
                    }
                } else {
                    $slider.find("iframe").attr("src", "");
                    var videos = $slider.find("video");
                    $(videos).each(function() {
                        $(this)[0].pause();
                    });
                }
            });
        }

        // var $slider = $('.product-slider .main-slider');
        // console.log($slider.length);
        // if ($slider.length>0) {
        //     console.log("enter here");
        //     $('.product-slider .main-slider').on('beforeChange', function(slick, currentSlide) {
        //         console.log("before change called");
        //     });
        //     $slider.on('afterChange', function(slick, currentSlide) {
        //         console.log("enter in event");
        //         $slide = $slider.find(".slick-active");
        //         var type = $slide.find(".video").attr("data-type");
        //         var video = "";
        //         console.log(type);
        //         if(type){
        //             if(type=="youtube"){
        //                 video = $slide.find(".video").attr("data-video");
        //                 $slide.find("iframe").attr("src",video);
        //                 var videos = $slider.find("video");
        //                 $(videos).each(function(){
        //                     $(this)[0].pause();
        //                 })
        //             }
        //             else if(type=="mp4"){
        //                 $slider.find("iframe").attr("src","");
        //                 $slide.find("video")[0].play();
        //             }
        //             else{
        //                 $slider.find("iframe").attr("src",video);
        //                 slider.find("video")[0].pause();
        //             }
        //         }
        //         else{
        //             $slider.find("iframe").attr("src","");
        //             var videos = $slider.find("video");
        //             $(videos).each(function(){
        //                 $(this)[0].pause();
        //             });
        //         }
        //     });
        // }




        // .mp-menu ul li > 
        $('.mp-menu a.icon').on('click', function(e) {
            e.preventDefault();
            // $('.mp-level.mp-level-open').slideUp('slow');

            // $('.mp-level').parent("li").slideUp(400);
            // $(".mp-level.mp-level-open").animate({ scrollTop: 0 }, 600);
            $("window").scrollTop(0);
        });


        // $( '#mp-menu' ).multilevelpushmenu({
        //     direction: 'rtl',
        //     backItemIcon: 'fa fa-angle-left',
        //     groupIcon: 'fa fa-angle-right'
        // });

        // $('.trigger-search').on('click', function(e) {
        //     e.preventDefault();
        //   $('.right-links').addClass('active');
        // });

        $('.close-icon').on('click', function(e) {
            e.preventDefault();
            $('.mp-pusher').removeClass('mp-pushed').css("transform", "translate3d(0px, 0px, 0px)");

        });

        // newcode

        $(".mp-menu .close-icon").on("click", function(e) {
            e.preventDefault();
            $("body").removeClass("menu-opened");
        });

        $(".mp-menu .close-icon").on("click", function(e) {
            e.preventDefault();
            $(".mp-level").removeClass("mp-level-open");
        });
        // newcode

        $(document).on("click", ".search a.icon", function(e) {
            e.preventDefault();
            $("body").addClass("active-search");
        });

        $(".search .close-icon").on("click", function(e) {
            e.preventDefault();
            $("body").removeClass("active-search");
        });

        // $(".toggle-filter-btn").on("click", function(e) {
        //     e.preventDefault();
        //     if ($(window).width() <= 768) {
        //         $(this).parent().toggleClass("active");
        //         $(this).parent().find(".filters").slideToggle();
        //     }
        // });

        $(".toggle-filter-btn").on("click", function(e) {
            e.preventDefault();
            if ($(window).width() <= 768) {
                $("body").addClass("active-filter");
            }
        });
        $(".close-filter").click(function(e) {
            $("body").removeClass("active-filter");
        });

        $(".scroll-up").on("click", function(e) {
            e.preventDefault();
            $("html, body").animate({ scrollTop: 0 }, 600);
        });

        $(".nav-links li.has-submenu").on("mouseenter", function(e) {
            e.preventDefault();
            $(this).addClass("active");
        });

        $(".nav-links li.has-submenu").on("mouseleave", function(e) {
            e.preventDefault();
            $(this).removeClass("active");
        });


        // product slick custom slick index 7 july 2021
        $(document).on('click', '.product-slider .thumb-slider .slick-slide', function(e) {
            // alert('hello aim');
            var activeSlide = $(this).data('slick-index');
            // alert(activeSlide);
            var slider = $(".main-slider.slick-instance");
            slider.slick("slickGoTo", activeSlide); 

        });
            // for full remvew popup solider
            $(document).on('click', '.full-reviews .thumb-slider .slick-slide', function(e) {
                // alert('hello aim');
                var activeSlide = $(this).data('slick-index');
                // alert(activeSlide);
                var slider = $(".main-slider.slick-instance");
                slider.slick("slickGoTo", activeSlide); 

            });
            // for full remvew popup solider
        // product slick custom slick index 7 july 2021

        //slick  slider 
        $(".slick-instance").slick();

        // mCustomScrollbar
        $(".inner-scroll").mCustomScrollbar();
        $(".touchspin").TouchSpin({
            min: 1
        });

        // review-dropdown
        $('.review-rating .dropdown ').hover(function() {



            // Add the class .open and show the menu

            $('.review-rating .dropdown').addClass('show');

        }, function() {

            // Sets the timer variable to run the timeout delay
            timer = setTimeout(function() {
                // remove the class .open and hide the submenu
                $('.review-rating .dropdown').removeClass("show");
            }, 300);

        });


        if ($(window).width() <= 991) {
            $(".review-rating .dropdown .dropdown-toggle").on("click", function(e) {
                e.preventDefault();

                $('.review-rating .dropdown .myaccount-dropdown').toggleClass('open-on-lg');


            });
        }
        // review-dropdown

        $('.cart .dropdown ').hover(function() {



            // Add the class .open and show the menu

            $('.cart .dropdown').addClass('show');

        }, function() {

            // Sets the timer variable to run the timeout delay
            timer = setTimeout(function() {
                // remove the class .open and hide the submenu
                $('.cart .dropdown').removeClass("show");
            }, 300);

        });


        if ($(window).width() <= 991) {
            $(".cart .dropdown .dropdown-toggle").on("click", function(e) {
                e.preventDefault();

                $('.cart .dropdown .cart-dropdown').toggleClass('open-on-lg');


            });
        }

        // accountdropdown

        $('.login-dropdown .dropdown ').hover(function() {



            // Add the class .open and show the menu

            $('.login-dropdown .dropdown').addClass('show');

        }, function() {

            // Sets the timer variable to run the timeout delay
            timer = setTimeout(function() {
                // remove the class .open and hide the submenu
                $('.login-dropdown .dropdown').removeClass("show");
            }, 300);

        });

        if ($(window).width() <= 991) {
            $(".login-dropdown .dropdown .dropdown-toggle ").on("click", function(e) {
                e.preventDefault();

                $('.login-dropdown .dropdown .myaccount-dropdown').toggleClass('open-on-lg');

            });
        }

        // $( '#menu' ).multilevelpushmenu({
        //     containersToPush: [$( '#pushobj' )],
        //     mode: 'cover'
        // });

        // $(".menu-icon").click( function(e) {
        //   e.preventDefault();
        //   $("body").toggleClass("menu-active");
        // });

        // $(document).on("click", ".cd-navigation .btn-default", function() {
        //     if($("body").hasClass("menu-active")) {
        //     $("body").removeClass("menu-active");
        //     $(".cd-lateral-nav").removeClass("lateral-menu-is-open");
        //     $(".wrapper").removeClass("lateral-menu-is-open");
        // }
    }




};
var lazyload = {
    load: function(wrapper, dataURL) {
        $(".marker-end")
            .on('lazyshow', function() {
                if ($("#loadmorecount").val() < 3) {
                    $.ajax({
                        url: dataURL,
                        dataType: "html",
                        success: function(responseText) {
                            setTimeout(function() {
                                if (responseText != "") {
                                    $(wrapper).append($.parseHTML(responseText));
                                    $(window).lazyLoadXT();
                                    $('.marker-end').lazyLoadXT({ visibleOnly: false, checkDuplicates: false });
                                } else {
                                    $('.marker-end').hide();
                                }
                            }, 700);

                        },
                        complete: function() {
                            $("#loadmorecount").val(parseInt($("#loadmorecount").val()) + 1);
                        }
                    })
                } else {
                    $('.marker-end').hide();
                }
            })
            .lazyLoadXT({ visibleOnly: false });
    }
};