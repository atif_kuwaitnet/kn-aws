<?php $ishome = 1; include_once "includes/header.php";?>
<!-- <div class="pre-loader-wrapper">
	<div class="logo">
		<img src="src/images/logo.png" alt="Olshi">
	</div>
	<div class='loader'>    
		<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="43px" height="43px" viewBox="0 0 128 128" xml:space="preserve"><rect x="0" y="0" width="100%" height="100%" fill="#FFFFFF" /><g><linearGradient id="linear-gradient"><stop offset="0%" stop-color="#860840" fill-opacity="1"/><stop offset="100%" stop-color="#bc7595" fill-opacity="0.56"/></linearGradient><linearGradient id="linear-gradient2"><stop offset="0%" stop-color="#860840" fill-opacity="1"/><stop offset="100%" stop-color="#e8d1db" fill-opacity="0.19"/></linearGradient><path d="M64 .98A63.02 63.02 0 1 1 .98 64 63.02 63.02 0 0 1 64 .98zm0 15.76A47.26 47.26 0 1 1 16.74 64 47.26 47.26 0 0 1 64 16.74z" fill-rule="evenodd" fill="url(#linear-gradient)"/><path d="M64.12 125.54A61.54 61.54 0 1 1 125.66 64a61.54 61.54 0 0 1-61.54 61.54zm0-121.1A59.57 59.57 0 1 0 123.7 64 59.57 59.57 0 0 0 64.1 4.43zM64 115.56a51.7 51.7 0 1 1 51.7-51.7 51.7 51.7 0 0 1-51.7 51.7zM64 14.4a49.48 49.48 0 1 0 49.48 49.48A49.48 49.48 0 0 0 64 14.4z" fill-rule="evenodd" fill="url(#linear-gradient2)"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1800ms" repeatCount="indefinite"></animateTransform></g></svg>
	</div>
</div>      -->
    <div class="content-wrapper">

        <div class="main-banner slick-instance" data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "fade": true, "infinite": false, "arrows": true, "dots": true, "autoplay": true, "autoplaySpeed": 8000, "centerMode": false}'>
			
			<div class="image">
				<img src="src/images/main-banner.png" alt="main-banner">
				<div class="container">
					<div class="caption">
						<h1>AWS Announces New Investments in Germany</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
					</div>
				</div>
			</div>
			<div class="image">
				<img src="src/images/main-banner.png" alt="main-banner">
				<div class="caption">
					<h1>AWS Announces New Investments in Germany</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
				</div>
			</div>
			<div class="image">
				<img src="src/images/main-banner.png" alt="main-banner">
				<div class="caption">
					<h1>AWS Announces New Investments in Germany</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
				</div>
			</div>
			
		</div>
		
		<div class="main-services slick-instance" data-slick='{"slidesToShow": 3, "slidesToScroll": 1, "fade": false, "infinite": true, "centerMode": false, "arrows": true, "dots": false ,
	            "responsive": [
	                {"breakpoint": 992, "settings": {"slidesToShow": 2}},{"breakpoint": 600, "settings": {"slidesToShow": 2}} ,{"breakpoint": 576, "settings": {"slidesToShow": 1}}
	                ]}'>
			<div class="slide">
				<div class="service-schedule">
					<div class="image">
						<img src="src/images/service-1.png" alt="service">
						<div class="caption">
							<div class="title">
								AWS Service Sprawl Starts To Hurt The Cloud Ecosystem
							</div>
							<div class="date">
								19 December
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="service-schedule">
					<div class="image">
						<img src="src/images/service-2.png" alt="service">
						<div class="caption">
							<div class="title">
								AWS Service Sprawl Starts To Hurt The Cloud Ecosystem
							</div>
							<div class="date">
								19 December
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="service-schedule">
					<div class="image">
						<img src="src/images/service-1.png" alt="service">
						<div class="caption">
							<div class="title">
								AWS Service Sprawl Starts To Hurt The Cloud Ecosystem
							</div>
							<div class="date">
								19 December
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="aws-practices">
			<div class="container">
				<div class="section-title">
					KuwaitNet Capital AWS Practices
				</div>
				<p class="section-subtitle">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh
				</p>

				<div class="text-center">
					<ul class="nav nav-pills" id="pills-tab" role="tablist">
						<li class="nav-item" >
							<a class="nav-link active" data-toggle="pill" href="#practice1" role="tab" aria-controls="pills-practice1" aria-selected="true">Practice 1</a>
						</li>
						<li class="nav-item" >
							<a class="nav-link" data-toggle="pill" href="#practice2" role="tab" aria-controls="pills-practice2" aria-selected="false">Practice 2</a>
						</li>
						<li class="nav-item" >
							<a class="nav-link" data-toggle="pill" href="#practice3" role="tab" aria-controls="pills-practice3" aria-selected="false">Practice 3</a>
						</li>
						<li class="nav-item" >
							<a class="nav-link" data-toggle="pill" href="#practice4" role="tab" aria-controls="pills-practice4" aria-selected="false">Practice 4</a>
						</li>
					</ul>
				</div>

				<div class="tab-content mt-20">
					<div class="tab-pane fade show active" id="practice1" role="tabpanel" aria-labelledby="pills-practice1-tab">
						<div class="steps-wrapper">
							<ul class="step">
								<li>
									<span class="counter">01</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/green-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="step-counter">STEP 01</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									<span class="icon">
										<img src="src/images/icon/step-icon-1.png" alt="step icon">
									</span>
								</li>
							</ul>
							<ul class="step reverse">
								<li>
									<span class="counter">02</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/blue-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="icon">
										<img src="src/images/icon/step-icon-2.png" alt="step icon">
									</span>
									<span class="step-counter">STEP 02</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									
								</li>
							</ul>
							<ul class="step">
								<li>
									<span class="counter">03</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/green-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="step-counter">STEP 03</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									<span class="icon">
										<img src="src/images/icon/step-icon-3.png" alt="step icon">
									</span>
								</li>
							</ul>
							<ul class="step reverse">
								<li>
									<span class="counter">04</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/blue-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="icon">
										<img src="src/images/icon/step-icon-4.png" alt="step icon">
									</span>
									<span class="step-counter">STEP 04</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									
								</li>
							</ul>
						</div>
					</div>
					<div class="tab-pane fade" id="practice2" role="tabpanel" aria-labelledby="pills-practice2-tab">
						<div class="steps-wrapper">
							<ul class="step">
								<li>
									<span class="counter">01</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/green-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="step-counter">STEP 01</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									<span class="icon">
										<img src="src/images/icon/step-icon-1.png" alt="step icon">
									</span>
								</li>
							</ul>
							<ul class="step reverse">
								<li>
									<span class="counter">02</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/blue-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="icon">
										<img src="src/images/icon/step-icon-2.png" alt="step icon">
									</span>
									<span class="step-counter">STEP 02</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									
								</li>
							</ul>
							<ul class="step">
								<li>
									<span class="counter">03</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/green-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="step-counter">STEP 03</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									<span class="icon">
										<img src="src/images/icon/step-icon-3.png" alt="step icon">
									</span>
								</li>
							</ul>
							<ul class="step reverse">
								<li>
									<span class="counter">04</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/blue-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="icon">
										<img src="src/images/icon/step-icon-4.png" alt="step icon">
									</span>
									<span class="step-counter">STEP 04</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									
								</li>
							</ul>
						</div>
					</div>
					<div class="tab-pane fade" id="practice3" role="tabpanel" aria-labelledby="pills-practice3-tab">
						<div class="steps-wrapper">
							<ul class="step">
								<li>
									<span class="counter">01</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/green-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="step-counter">STEP 01</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									<span class="icon">
										<img src="src/images/icon/step-icon-1.png" alt="step icon">
									</span>
								</li>
							</ul>
							<ul class="step reverse">
								<li>
									<span class="counter">02</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/blue-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="icon">
										<img src="src/images/icon/step-icon-2.png" alt="step icon">
									</span>
									<span class="step-counter">STEP 02</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									
								</li>
							</ul>
							<ul class="step">
								<li>
									<span class="counter">03</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/green-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="step-counter">STEP 03</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									<span class="icon">
										<img src="src/images/icon/step-icon-3.png" alt="step icon">
									</span>
								</li>
							</ul>
							<ul class="step reverse">
								<li>
									<span class="counter">04</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/blue-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="icon">
										<img src="src/images/icon/step-icon-4.png" alt="step icon">
									</span>
									<span class="step-counter">STEP 04</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									
								</li>
							</ul>
						</div>
					</div>
					<div class="tab-pane fade" id="practice4" role="tabpanel" aria-labelledby="pills-practice4-tab">
						<div class="steps-wrapper">
							<ul class="step">
								<li>
									<span class="counter">01</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/green-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="step-counter">STEP 01</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									<span class="icon">
										<img src="src/images/icon/step-icon-1.png" alt="step icon">
									</span>
								</li>
							</ul>
							<ul class="step reverse">
								<li>
									<span class="counter">02</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/blue-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="icon">
										<img src="src/images/icon/step-icon-2.png" alt="step icon">
									</span>
									<span class="step-counter">STEP 02</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									
								</li>
							</ul>
							<ul class="step">
								<li>
									<span class="counter">03</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/green-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="step-counter">STEP 03</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									<span class="icon">
										<img src="src/images/icon/step-icon-3.png" alt="step icon">
									</span>
								</li>
							</ul>
							<ul class="step reverse">
								<li>
									<span class="counter">04</span>
									<h4>Title</h4>
									<p>Lorem ipsum dolorsit </p>
								</li>
								<li class="arrow">
									<img src="src/images/blue-arrow.png" alt="step icon">
								</li>
								<li>
									<span class="icon">
										<img src="src/images/icon/step-icon-4.png" alt="step icon">
									</span>
									<span class="step-counter">STEP 04</span>
									<p>Lorem ipsum dolorsit amet,consectetuer adipiscing eladipiscing </p>
									
								</li>
							</ul>
						</div>
					</div>
				</div>
				


			</div>
		</div>

		<div class="case-studies slick-instance" data-slick='{"slidesToShow": 4, "slidesToScroll": 1, "fade": false, "infinite": true, "centerMode": false, "arrows": true, "dots": false ,
	            "responsive": [
	                {"breakpoint": 992, "settings": {"slidesToShow": 3}},{"breakpoint": 600, "settings": {"slidesToShow": 2}} ,{"breakpoint": 576, "settings": {"slidesToShow": 1}}
	                ]}'>
			<div class="slide">
				<div class="service-schedule">
					<div class="image">
						<img src="src/images/casestudyimg.png" alt="service">
						<div class="caption">
							<div class="title">
								Case Study 1
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="service-schedule">
					<div class="image">
						<img src="src/images/casestudyimg.png" alt="service">
						<div class="caption">
							<div class="title">
								Case Study 2
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="service-schedule">
					<div class="image">
						<img src="src/images/casestudyimg.png" alt="service">
						<div class="caption">
							<div class="title">
								Case Study 3
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="service-schedule">
					<div class="image">
						<img src="src/images/casestudyimg.png" alt="service">
						<div class="caption">
							<div class="title">
								Case Study 4
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<div class="service-schedule">
					<div class="image">
						<img src="src/images/service-1.png" alt="service">
						<div class="caption">
							<div class="title">
								Case Study 5
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="programs">
			<div class="container">

				<div class="section-title">
					KuwaitNet AWS Programs
				</div>

				<p class="section-subtitle">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh
				</p>

				<div class="row justify-content-center col-gap-40">
					<div class="col-md-4 col-6">
						<div class="program">
							<div class="icon">
								<svg width="58px" height="58px">
									<defs>
										<filter id="Filter_0">
											<feFlood flood-color="rgb(166, 197, 59)" flood-opacity="1" result="floodOut" />
											<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
											<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
										</filter>
									</defs>
									<g filter="url(#Filter_0)">
										<path fill-rule="evenodd"  fill="rgb(67, 203, 131)" d="M53.000,11.898 L53.000,30.100 C55.832,30.580 58.000,33.033 58.000,35.999 C58.000,39.308 55.308,41.999 52.000,41.999 C50.888,41.999 49.848,41.695 48.957,41.164 L41.165,48.956 C41.695,49.847 42.000,50.887 42.000,51.999 C42.000,55.307 39.309,57.999 36.000,57.999 C33.034,57.999 30.581,55.831 30.101,52.999 L11.899,52.999 C11.419,55.831 8.966,57.999 6.000,57.999 C2.691,57.999 -0.000,55.307 -0.000,51.999 C-0.000,49.033 2.168,46.579 5.000,46.100 L5.000,27.898 C2.168,27.418 -0.000,24.965 -0.000,21.999 C-0.000,18.690 2.691,15.999 6.000,15.999 C7.294,15.999 8.477,16.430 9.455,17.130 L17.120,9.465 C16.421,8.484 16.000,7.293 16.000,5.999 C16.000,2.690 18.691,-0.001 22.000,-0.001 C24.966,-0.001 27.419,2.167 27.899,4.999 L46.101,4.999 C46.581,2.167 49.034,-0.001 52.000,-0.001 C55.308,-0.001 58.000,2.690 58.000,5.999 C58.000,8.965 55.832,11.418 53.000,11.898 ZM56.000,35.999 C56.000,33.793 54.206,31.999 52.000,31.999 C49.794,31.999 48.000,33.793 48.000,35.999 C48.000,38.204 49.794,39.999 52.000,39.999 C54.206,39.999 56.000,38.204 56.000,35.999 ZM37.000,27.898 L37.000,34.999 L37.991,34.999 C38.543,34.999 38.991,35.447 38.991,35.999 C38.991,36.551 38.543,36.999 37.991,36.999 L37.000,36.999 L37.000,46.100 C38.079,46.283 39.059,46.755 39.863,47.431 L47.431,39.862 C46.756,39.059 46.284,38.079 46.101,36.999 L45.991,36.999 C45.438,36.999 44.991,36.551 44.991,35.999 C44.991,35.447 45.438,34.999 45.991,34.999 L46.091,34.999 C46.515,32.497 48.494,30.525 51.000,30.100 L51.000,11.908 C50.092,11.754 49.257,11.390 48.537,10.876 L40.880,18.533 C41.579,19.514 42.000,20.705 42.000,21.999 C42.000,24.965 39.832,27.418 37.000,27.898 ZM40.000,21.999 C40.000,19.794 38.205,17.999 36.000,17.999 C33.794,17.999 32.000,19.794 32.000,21.999 C32.000,24.204 33.794,25.999 36.000,25.999 C38.205,25.999 40.000,24.204 40.000,21.999 ZM36.000,55.999 C38.205,55.999 40.000,54.204 40.000,51.999 C40.000,49.794 38.205,47.999 36.000,47.999 C33.794,47.999 32.000,49.794 32.000,51.999 C32.000,54.204 33.794,55.999 36.000,55.999 ZM2.000,51.999 C2.000,54.204 3.794,55.999 6.000,55.999 C8.205,55.999 10.000,54.204 10.000,51.999 C10.000,49.794 8.205,47.999 6.000,47.999 C3.794,47.999 2.000,49.794 2.000,51.999 ZM6.000,17.999 C3.794,17.999 2.000,19.794 2.000,21.999 C2.000,24.204 3.794,25.999 6.000,25.999 C8.205,25.999 10.000,24.204 10.000,21.999 C10.000,19.794 8.205,17.999 6.000,17.999 ZM7.000,27.908 L7.000,46.090 C7.906,46.243 8.739,46.606 9.457,47.118 L9.647,46.929 C10.038,46.538 10.670,46.538 11.061,46.929 C11.452,47.319 11.452,47.952 11.061,48.343 L10.870,48.533 C11.382,49.255 11.745,50.090 11.899,50.999 L30.101,50.999 C30.525,48.494 32.497,46.514 35.000,46.090 L35.000,36.045 C34.999,36.029 34.991,36.015 34.991,35.999 C34.991,35.982 34.999,35.969 35.000,35.952 L35.000,27.908 C32.497,27.484 30.525,25.505 30.101,22.999 L11.899,22.999 C11.474,25.505 9.503,27.484 7.000,27.908 ZM22.000,1.999 C19.794,1.999 18.000,3.793 18.000,5.999 C18.000,8.205 19.794,10.000 22.000,10.000 C24.205,10.000 26.000,8.205 26.000,5.999 C26.000,3.793 24.205,1.999 22.000,1.999 ZM46.101,6.999 L27.899,6.999 C27.474,9.508 25.498,11.488 22.991,11.909 L22.991,11.999 C22.991,12.552 22.543,12.999 21.991,12.999 C21.438,12.999 20.991,12.552 20.991,11.999 L20.991,11.907 C20.088,11.750 19.253,11.392 18.534,10.879 L10.864,18.549 C11.373,19.269 11.746,20.094 11.899,20.999 L20.991,20.999 L20.991,19.999 C20.991,19.447 21.438,18.999 21.991,18.999 C22.543,18.999 22.991,19.447 22.991,19.999 L22.991,20.999 L30.101,20.999 C30.580,18.167 33.034,15.999 36.000,15.999 C37.294,15.999 38.485,16.420 39.466,17.119 L47.126,9.459 C46.616,8.739 46.255,7.906 46.101,6.999 ZM52.000,1.999 C49.794,1.999 48.000,3.793 48.000,5.999 C48.000,8.205 49.794,10.000 52.000,10.000 C54.206,10.000 56.000,8.205 56.000,5.999 C56.000,3.793 54.206,1.999 52.000,1.999 ZM21.991,17.999 C21.438,17.999 20.991,17.551 20.991,16.999 L20.991,14.999 C20.991,14.447 21.438,13.999 21.991,13.999 C22.543,13.999 22.991,14.447 22.991,14.999 L22.991,16.999 C22.991,17.551 22.543,17.999 21.991,17.999 ZM13.902,44.088 C14.293,44.478 14.293,45.111 13.902,45.502 L12.766,46.638 C12.571,46.834 12.315,46.931 12.059,46.931 C11.803,46.931 11.547,46.834 11.352,46.638 C10.961,46.248 10.961,45.615 11.352,45.224 L12.488,44.088 C12.879,43.697 13.512,43.697 13.902,44.088 ZM14.192,42.383 L15.329,41.247 C15.719,40.856 16.352,40.856 16.743,41.247 C17.133,41.638 17.133,42.271 16.743,42.661 L15.606,43.797 C15.411,43.993 15.155,44.090 14.899,44.090 C14.644,44.090 14.388,43.993 14.192,43.797 C13.802,43.407 13.802,42.774 14.192,42.383 ZM17.033,39.543 L17.115,39.460 C16.419,38.480 16.000,37.290 16.000,35.999 C16.000,33.036 18.163,30.584 20.991,30.101 L20.991,29.999 C20.991,29.447 21.438,28.999 21.991,28.999 C22.543,28.999 22.991,29.447 22.991,29.999 L22.991,30.089 C25.498,30.510 27.474,32.491 27.899,34.999 L27.991,34.999 C28.543,34.999 28.991,35.447 28.991,35.999 C28.991,36.551 28.543,36.999 27.991,36.999 L27.899,36.999 C27.419,39.831 24.966,41.999 22.000,41.999 C20.704,41.999 19.511,41.577 18.529,40.875 L18.447,40.958 C18.251,41.152 17.995,41.250 17.740,41.250 C17.484,41.250 17.227,41.152 17.032,40.957 C16.642,40.566 16.642,39.933 17.033,39.543 ZM22.000,39.999 C24.195,39.999 25.980,38.221 25.997,36.029 C25.996,36.018 25.991,36.009 25.991,35.999 C25.991,35.988 25.996,35.979 25.997,35.968 C25.980,33.777 24.195,31.999 22.000,31.999 C19.794,31.999 18.000,33.793 18.000,35.999 C18.000,38.204 19.794,39.999 22.000,39.999 ZM30.991,34.999 L32.991,34.999 C33.543,34.999 33.991,35.447 33.991,35.999 C33.991,36.551 33.543,36.999 32.991,36.999 L30.991,36.999 C30.438,36.999 29.991,36.551 29.991,35.999 C29.991,35.447 30.438,34.999 30.991,34.999 ZM21.991,27.999 C21.438,27.999 20.991,27.551 20.991,26.999 L20.991,24.999 C20.991,24.447 21.438,23.999 21.991,23.999 C22.543,23.999 22.991,24.447 22.991,24.999 L22.991,26.999 C22.991,27.551 22.543,27.999 21.991,27.999 ZM43.991,35.999 C43.991,36.551 43.543,36.999 42.991,36.999 L40.991,36.999 C40.438,36.999 39.991,36.551 39.991,35.999 C39.991,35.447 40.438,34.999 40.991,34.999 L42.991,34.999 C43.543,34.999 43.991,35.447 43.991,35.999 Z"/>
									</g>
								</svg>
							</div>
							<div class="title">
								Program 1
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
						</div>
					</div>
					<div class="col-md-4 col-6">
						<div class="program">
							<div class="icon">
								<svg width="56px" height="56px" fill="#d64b74">
									<defs>
										<filter id="Filter_0">
											<feFlood flood-color="rgb(214, 75, 116)" flood-opacity="1" result="floodOut" />
											<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
											<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
										</filter>

									</defs>
									<g fill="#d64b74">
										<path fill-rule="evenodd"  fill="rgb(214, 75, 116)" d="M55.513,33.183 C53.037,46.403 41.466,55.999 28.000,55.999 C14.534,55.999 2.963,46.403 0.487,33.183 C0.163,31.459 -0.000,29.715 -0.000,27.999 C-0.000,12.560 12.560,-0.001 28.000,-0.001 C43.439,-0.001 56.000,12.560 56.000,27.999 C56.000,29.715 55.836,31.459 55.513,33.183 ZM28.000,54.000 C40.098,54.000 50.548,45.658 53.297,33.999 L35.998,33.999 C34.112,36.514 31.161,37.999 28.000,37.999 C24.839,37.999 21.888,36.514 20.001,33.999 L2.703,33.999 C5.452,45.658 15.902,54.000 28.000,54.000 ZM28.000,1.999 C13.663,1.999 2.000,13.662 2.000,27.999 C2.000,29.322 2.105,30.663 2.313,31.999 L20.518,31.999 C20.851,31.999 21.163,32.166 21.349,32.443 C22.839,34.669 25.326,35.999 28.000,35.999 C30.674,35.999 33.161,34.669 34.651,32.443 C34.837,32.166 35.149,31.999 35.482,31.999 L53.687,31.999 C53.895,30.663 54.000,29.322 54.000,27.999 C54.000,13.662 42.336,1.999 28.000,1.999 ZM51.000,28.999 L47.000,28.999 C46.448,28.999 46.000,28.552 46.000,27.999 C46.000,27.447 46.448,26.999 47.000,26.999 L51.000,26.999 C51.552,26.999 52.000,27.447 52.000,27.999 C52.000,28.552 51.552,28.999 51.000,28.999 ZM48.418,17.365 L44.954,19.365 C44.797,19.456 44.625,19.499 44.455,19.499 C44.110,19.499 43.774,19.320 43.588,18.999 C43.312,18.521 43.476,17.909 43.954,17.632 L47.418,15.633 C47.895,15.355 48.508,15.521 48.785,15.999 C49.060,16.477 48.897,17.089 48.418,17.365 ZM38.366,12.044 C38.181,12.365 37.845,12.545 37.499,12.545 C37.329,12.545 37.157,12.502 37.000,12.411 C36.521,12.135 36.358,11.523 36.634,11.044 L38.634,7.581 C38.910,7.102 39.521,6.939 40.000,7.214 C40.478,7.491 40.642,8.102 40.366,8.581 L38.366,12.044 ZM38.868,17.131 C39.259,17.522 39.259,18.154 38.868,18.544 L31.433,25.980 C31.784,26.575 32.000,27.259 32.000,27.999 C32.000,30.204 30.205,31.999 28.000,31.999 C25.794,31.999 24.000,30.204 24.000,27.999 C24.000,25.793 25.794,23.999 28.000,23.999 C28.739,23.999 29.424,24.215 30.019,24.566 L37.454,17.131 C37.845,16.740 38.477,16.740 38.868,17.131 ZM28.000,25.999 C26.897,25.999 26.000,26.896 26.000,27.999 C26.000,29.102 26.897,29.999 28.000,29.999 C29.103,29.999 30.000,29.102 30.000,27.999 C30.000,26.896 29.103,25.999 28.000,25.999 ZM28.000,9.999 C27.448,9.999 27.000,9.551 27.000,9.000 L27.000,4.999 C27.000,4.447 27.448,3.999 28.000,3.999 C28.552,3.999 29.000,4.447 29.000,4.999 L29.000,9.000 C29.000,9.551 28.552,9.999 28.000,9.999 ZM19.000,12.411 C18.843,12.502 18.670,12.545 18.501,12.545 C18.155,12.545 17.819,12.365 17.634,12.044 L15.634,8.581 C15.358,8.102 15.521,7.491 16.000,7.214 C16.478,6.939 17.089,7.102 17.366,7.581 L19.366,11.044 C19.642,11.523 19.478,12.135 19.000,12.411 ZM11.544,19.499 C11.375,19.499 11.203,19.456 11.045,19.365 L7.581,17.365 C7.103,17.089 6.939,16.477 7.215,15.999 C7.492,15.521 8.102,15.355 8.581,15.633 L12.045,17.632 C12.524,17.909 12.687,18.521 12.412,18.999 C12.226,19.320 11.890,19.499 11.544,19.499 ZM10.000,27.999 C10.000,28.552 9.552,28.999 9.000,28.999 L5.000,28.999 C4.448,28.999 4.000,28.552 4.000,27.999 C4.000,27.447 4.448,26.999 5.000,26.999 L9.000,26.999 C9.552,26.999 10.000,27.447 10.000,27.999 Z"/>
									</g>
								</svg>
							</div>
							<div class="title">
								Program 2
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
						</div>
					</div>
					<div class="col-md-4 col-6">
						<div class="program">
							<div class="icon">
								<svg width="60px" height="48px">
									<defs>
									<filter id="Filter_0">
										<feFlood flood-color="rgb(155, 209, 235)" flood-opacity="1" result="floodOut" />
										<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
										<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
									</filter>

									</defs>
									<g>
									<path fill-rule="evenodd"  fill="rgb(155, 209, 235)"
									d="M57.000,47.999 L3.000,47.999 C1.346,47.999 -0.000,46.653 -0.000,44.999 L-0.000,2.999 C-0.000,1.345 1.346,-0.001 3.000,-0.001 L57.000,-0.001 C58.654,-0.001 60.000,1.345 60.000,2.999 L60.000,44.999 C60.000,46.653 58.654,47.999 57.000,47.999 ZM58.000,2.999 C58.000,2.447 57.552,1.999 57.000,1.999 L3.000,1.999 C2.448,1.999 2.000,2.447 2.000,2.999 L2.000,44.999 C2.000,45.551 2.448,45.999 3.000,45.999 L57.000,45.999 C57.552,45.999 58.000,45.551 58.000,44.999 L58.000,2.999 ZM53.000,41.999 L41.000,41.999 C40.447,41.999 40.000,41.552 40.000,40.999 C40.000,40.447 40.447,39.999 41.000,39.999 L53.000,39.999 C53.553,39.999 54.000,40.447 54.000,40.999 C54.000,41.552 53.553,41.999 53.000,41.999 ZM53.000,33.999 L37.000,33.999 C36.447,33.999 36.000,33.551 36.000,32.999 C36.000,32.446 36.447,31.999 37.000,31.999 L53.000,31.999 C53.553,31.999 54.000,32.446 54.000,32.999 C54.000,33.551 53.553,33.999 53.000,33.999 ZM53.000,29.999 L37.000,29.999 C36.447,29.999 36.000,29.552 36.000,28.999 C36.000,28.446 36.447,27.999 37.000,27.999 L53.000,27.999 C53.553,27.999 54.000,28.446 54.000,28.999 C54.000,29.552 53.553,29.999 53.000,29.999 ZM53.000,25.999 L37.000,25.999 C36.447,25.999 36.000,25.552 36.000,24.999 C36.000,24.447 36.447,23.999 37.000,23.999 L53.000,23.999 C53.553,23.999 54.000,24.447 54.000,24.999 C54.000,25.552 53.553,25.999 53.000,25.999 ZM53.000,21.999 L37.000,21.999 C36.447,21.999 36.000,21.552 36.000,20.999 C36.000,20.446 36.447,19.999 37.000,19.999 L53.000,19.999 C53.553,19.999 54.000,20.446 54.000,20.999 C54.000,21.552 53.553,21.999 53.000,21.999 ZM53.000,17.999 L37.000,17.999 C36.447,17.999 36.000,17.552 36.000,16.999 C36.000,16.446 36.447,15.999 37.000,15.999 L53.000,15.999 C53.553,15.999 54.000,16.446 54.000,16.999 C54.000,17.552 53.553,17.999 53.000,17.999 ZM53.000,13.999 L37.000,13.999 C36.447,13.999 36.000,13.552 36.000,12.999 C36.000,12.447 36.447,11.999 37.000,11.999 L53.000,11.999 C53.553,11.999 54.000,12.447 54.000,12.999 C54.000,13.552 53.553,13.999 53.000,13.999 ZM47.000,9.999 L37.000,9.999 C36.447,9.999 36.000,9.552 36.000,8.999 C36.000,8.446 36.447,7.999 37.000,7.999 L47.000,7.999 C47.553,7.999 48.000,8.446 48.000,8.999 C48.000,9.552 47.553,9.999 47.000,9.999 ZM35.000,41.999 L23.000,41.999 C22.447,41.999 22.000,41.552 22.000,40.999 C22.000,40.447 22.447,39.999 23.000,39.999 L35.000,39.999 C35.553,39.999 36.000,40.447 36.000,40.999 C36.000,41.552 35.553,41.999 35.000,41.999 ZM19.000,33.999 C15.760,33.999 12.655,32.803 10.258,30.630 C7.551,28.159 6.000,24.650 6.000,20.999 C6.000,13.831 11.832,7.999 19.000,7.999 C26.168,7.999 32.000,13.831 32.000,20.999 C32.000,24.891 30.278,28.547 27.277,31.029 C24.946,32.945 22.008,33.999 19.000,33.999 ZM25.256,30.049 C24.389,29.512 23.194,29.082 21.828,28.844 C21.051,28.708 20.484,28.017 20.511,27.235 C20.531,26.906 20.530,26.587 20.530,26.259 C20.530,25.640 20.812,25.090 21.086,24.559 C21.305,24.134 21.531,23.694 21.551,23.317 C21.558,23.185 21.591,23.057 21.647,22.937 C21.899,22.411 22.050,21.881 22.050,21.518 C22.050,21.491 22.049,21.471 22.047,21.454 C21.860,21.227 21.782,20.926 21.838,20.632 C21.885,20.389 21.927,20.135 21.971,19.877 L22.065,19.324 C22.392,17.501 21.495,16.952 20.791,16.699 C20.694,16.667 20.501,16.620 20.262,16.560 C19.454,16.358 18.635,16.138 17.927,15.841 C17.839,16.241 17.653,16.593 17.227,16.777 C16.048,17.307 15.694,17.998 15.934,19.320 L16.029,19.877 C16.073,20.135 16.115,20.389 16.162,20.632 C16.218,20.926 16.140,21.227 15.953,21.454 C15.951,21.471 15.950,21.491 15.950,21.518 C15.950,21.881 16.100,22.411 16.352,22.937 C16.409,23.057 16.441,23.185 16.449,23.317 C16.468,23.681 16.677,24.081 16.898,24.505 C17.179,25.044 17.470,25.601 17.470,26.259 C17.470,26.587 17.469,26.906 17.488,27.204 C17.516,28.017 16.949,28.708 16.173,28.844 C14.810,29.082 13.616,29.510 12.750,30.045 C14.578,31.306 16.747,31.999 19.000,31.999 C21.233,31.999 23.421,31.312 25.256,30.049 ZM19.000,9.999 C12.934,9.999 8.000,14.934 8.000,20.999 C8.000,23.885 9.157,26.658 11.174,28.715 C12.239,27.914 13.722,27.287 15.475,26.940 C15.470,26.724 15.470,26.494 15.470,26.259 C15.470,26.090 15.278,25.725 15.125,25.431 C14.869,24.939 14.556,24.339 14.470,23.632 C14.217,23.068 13.950,22.291 13.950,21.518 C13.950,21.174 14.009,20.870 14.126,20.610 C14.102,20.479 14.080,20.344 14.058,20.209 L13.966,19.674 C13.592,17.611 14.276,16.091 16.003,15.154 C16.009,15.064 16.014,14.970 16.018,14.899 C16.049,14.278 16.084,13.576 16.775,13.304 C17.141,13.160 17.645,13.278 17.926,13.550 C18.433,14.040 19.996,14.433 20.748,14.620 C21.056,14.699 21.304,14.762 21.451,14.812 C23.520,15.557 24.455,17.327 24.034,19.676 L23.942,20.209 C23.920,20.344 23.897,20.479 23.874,20.610 C23.991,20.870 24.050,21.174 24.050,21.518 C24.050,22.291 23.783,23.068 23.530,23.632 C23.443,24.349 23.125,24.968 22.865,25.474 C22.731,25.732 22.530,26.124 22.530,26.259 C22.530,26.491 22.530,26.719 22.524,26.940 C24.281,27.287 25.768,27.916 26.833,28.721 C28.855,26.669 30.000,23.914 30.000,20.999 C30.000,14.934 25.065,9.999 19.000,9.999 ZM7.000,39.999 L17.000,39.999 C17.553,39.999 18.000,40.447 18.000,40.999 C18.000,41.552 17.553,41.999 17.000,41.999 L7.000,41.999 C6.447,41.999 6.000,41.552 6.000,40.999 C6.000,40.447 6.447,39.999 7.000,39.999 Z"/>
									</g>
								</svg>
							</div>
							<div class="title">
								Program 3
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
						</div>
					</div>
					<div class="col-md-4 col-6">
						<div class="program">
							<div class="icon">
								<svg width="56px" height="48px">
									<defs>
										<filter id="Filter_0">
											<feFlood flood-color="rgb(7, 49, 79)" flood-opacity="1" result="floodOut" />
											<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
											<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
										</filter>

									</defs>
									<g>
									<path fill-rule="evenodd"  fill="rgb(7, 49, 79)"
									d="M55.000,19.999 L53.867,19.999 L50.480,43.706 C50.131,46.154 48.003,47.999 45.531,47.999 L10.469,47.999 C7.997,47.999 5.869,46.154 5.519,43.706 L2.133,19.999 L1.000,19.999 C0.448,19.999 -0.000,19.551 -0.000,18.999 L-0.000,14.999 C-0.000,14.447 0.448,13.999 1.000,13.999 L29.586,13.999 L41.794,1.791 C42.972,0.612 45.028,0.612 46.206,1.791 C47.424,3.009 47.424,4.988 46.208,6.205 L38.414,13.999 L55.000,13.999 C55.552,13.999 56.000,14.447 56.000,14.999 L56.000,18.999 C56.000,19.551 55.552,19.999 55.000,19.999 ZM7.499,43.424 C7.709,44.892 8.986,45.999 10.469,45.999 L45.531,45.999 C47.014,45.999 48.291,44.892 48.501,43.424 L51.847,19.999 L32.000,19.999 L26.000,19.999 L4.153,19.999 L7.499,43.424 ZM2.000,15.999 L2.000,17.999 L3.000,17.999 L25.586,17.999 L26.739,16.846 L27.586,15.999 L2.000,15.999 ZM44.794,4.791 C45.230,4.354 45.230,3.644 44.794,3.207 C44.367,2.780 43.631,2.782 43.208,3.205 L28.414,17.999 L31.586,17.999 L33.904,15.681 L44.794,4.791 ZM54.000,15.999 L36.414,15.999 L34.414,17.999 L53.000,17.999 L54.000,17.999 L54.000,15.999 ZM30.000,25.999 C30.552,25.999 31.000,26.447 31.000,26.999 C31.000,27.551 30.552,27.999 30.000,27.999 L26.000,27.999 C25.448,27.999 25.000,27.551 25.000,26.999 C25.000,26.447 25.448,25.999 26.000,25.999 L30.000,25.999 ZM22.000,39.999 L18.000,39.999 C17.448,39.999 17.000,39.551 17.000,38.999 C17.000,38.447 17.448,37.999 18.000,37.999 L22.000,37.999 C22.552,37.999 23.000,38.447 23.000,38.999 C23.000,39.551 22.552,39.999 22.000,39.999 ZM22.000,33.999 L18.000,33.999 C17.448,33.999 17.000,33.551 17.000,33.000 C17.000,32.446 17.448,31.999 18.000,31.999 L22.000,31.999 C22.552,31.999 23.000,32.446 23.000,33.000 C23.000,33.551 22.552,33.999 22.000,33.999 ZM22.000,27.999 L18.000,27.999 C17.448,27.999 17.000,27.551 17.000,26.999 C17.000,26.447 17.448,25.999 18.000,25.999 L22.000,25.999 C22.552,25.999 23.000,26.447 23.000,26.999 C23.000,27.551 22.552,27.999 22.000,27.999 ZM14.000,39.999 L10.000,39.999 C9.448,39.999 9.000,39.551 9.000,38.999 C9.000,38.447 9.448,37.999 10.000,37.999 L14.000,37.999 C14.552,37.999 15.000,38.447 15.000,38.999 C15.000,39.551 14.552,39.999 14.000,39.999 ZM14.000,33.999 L10.000,33.999 C9.448,33.999 9.000,33.551 9.000,33.000 C9.000,32.446 9.448,31.999 10.000,31.999 L14.000,31.999 C14.552,31.999 15.000,32.446 15.000,33.000 C15.000,33.551 14.552,33.999 14.000,33.999 ZM14.000,27.999 L10.000,27.999 C9.448,27.999 9.000,27.551 9.000,26.999 C9.000,26.447 9.448,25.999 10.000,25.999 L14.000,25.999 C14.552,25.999 15.000,26.447 15.000,26.999 C15.000,27.551 14.552,27.999 14.000,27.999 ZM26.000,31.999 L30.000,31.999 C30.552,31.999 31.000,32.446 31.000,33.000 C31.000,33.551 30.552,33.999 30.000,33.999 L26.000,33.999 C25.448,33.999 25.000,33.551 25.000,33.000 C25.000,32.446 25.448,31.999 26.000,31.999 ZM26.000,37.999 L30.000,37.999 C30.552,37.999 31.000,38.447 31.000,38.999 C31.000,39.551 30.552,39.999 30.000,39.999 L26.000,39.999 C25.448,39.999 25.000,39.551 25.000,38.999 C25.000,38.447 25.448,37.999 26.000,37.999 ZM34.000,25.999 L38.000,25.999 C38.552,25.999 39.000,26.447 39.000,26.999 C39.000,27.551 38.552,27.999 38.000,27.999 L34.000,27.999 C33.448,27.999 33.000,27.551 33.000,26.999 C33.000,26.447 33.448,25.999 34.000,25.999 ZM34.000,31.999 L38.000,31.999 C38.552,31.999 39.000,32.446 39.000,33.000 C39.000,33.551 38.552,33.999 38.000,33.999 L34.000,33.999 C33.448,33.999 33.000,33.551 33.000,33.000 C33.000,32.446 33.448,31.999 34.000,31.999 ZM34.000,37.999 L38.000,37.999 C38.552,37.999 39.000,38.447 39.000,38.999 C39.000,39.551 38.552,39.999 38.000,39.999 L34.000,39.999 C33.448,39.999 33.000,39.551 33.000,38.999 C33.000,38.447 33.448,37.999 34.000,37.999 ZM42.000,25.999 L46.000,25.999 C46.552,25.999 47.000,26.447 47.000,26.999 C47.000,27.551 46.552,27.999 46.000,27.999 L42.000,27.999 C41.448,27.999 41.000,27.551 41.000,26.999 C41.000,26.447 41.448,25.999 42.000,25.999 ZM42.000,31.999 L46.000,31.999 C46.552,31.999 47.000,32.446 47.000,33.000 C47.000,33.551 46.552,33.999 46.000,33.999 L42.000,33.999 C41.448,33.999 41.000,33.551 41.000,33.000 C41.000,32.446 41.448,31.999 42.000,31.999 ZM42.000,37.999 L46.000,37.999 C46.552,37.999 47.000,38.447 47.000,38.999 C47.000,39.551 46.552,39.999 46.000,39.999 L42.000,39.999 C41.448,39.999 41.000,39.551 41.000,38.999 C41.000,38.447 41.448,37.999 42.000,37.999 Z"/>
									</g>
								</svg>
							</div>
							<div class="title">
								Program 4
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
						</div>
					</div>
					<div class="col-md-4 col-6">
						<div class="program">
							<div class="icon">
								<svg width="44px" height="56px">
									<defs>
									<filter id="Filter_0">
										<feFlood flood-color="rgb(61, 113, 129)" flood-opacity="1" result="floodOut" />
										<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
										<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
									</filter>

									</defs>
									<g>
									<path fill-rule="evenodd"  fill="rgb(61, 113, 129)" d="M34.151,46.998 L22.606,55.795 C22.427,55.931 22.214,55.999 22.000,55.999 C21.786,55.999 21.573,55.931 21.393,55.795 L9.849,46.998 C3.682,42.300 -0.000,34.865 -0.000,27.112 L-0.000,8.999 C-0.000,8.446 0.447,7.999 1.000,7.999 C14.392,7.999 21.181,0.417 21.247,0.340 C21.436,0.124 21.711,0.000 21.999,0.000 C21.999,0.000 22.000,0.000 22.001,0.000 C22.288,0.000 22.561,0.124 22.751,0.339 C22.819,0.415 29.661,7.999 43.000,7.999 C43.553,7.999 44.000,8.446 44.000,8.999 L44.000,27.112 C44.000,34.865 40.318,42.300 34.151,46.998 ZM42.000,9.986 C30.755,9.688 24.133,4.435 22.000,2.422 C19.867,4.435 13.245,9.688 2.000,9.986 L2.000,27.112 C2.000,34.245 5.388,41.085 11.061,45.407 L22.000,53.742 L32.938,45.407 C38.612,41.085 42.000,34.245 42.000,27.112 L42.000,9.986 ZM33.920,41.874 C33.912,41.882 33.910,41.893 33.902,41.901 C33.895,41.907 33.886,41.909 33.879,41.916 C33.208,42.594 32.493,43.233 31.728,43.817 L22.606,50.766 C22.584,50.783 22.559,50.792 22.535,50.804 C22.503,50.824 22.477,50.849 22.443,50.866 C22.428,50.874 22.411,50.871 22.395,50.877 C22.382,50.884 22.370,50.893 22.356,50.898 C22.241,50.942 22.122,50.970 22.000,50.970 C21.863,50.970 21.731,50.932 21.604,50.877 C21.597,50.874 21.589,50.875 21.582,50.872 C21.574,50.868 21.564,50.870 21.557,50.866 C21.529,50.852 21.508,50.829 21.483,50.813 C21.454,50.795 21.421,50.787 21.393,50.766 L12.272,43.817 C7.093,39.869 4.000,33.624 4.000,27.112 L4.000,26.999 L4.000,12.797 C4.000,12.285 4.388,11.855 4.898,11.803 C12.712,11.003 18.184,7.944 21.397,5.518 C21.408,5.509 21.421,5.507 21.434,5.504 C21.473,5.476 21.509,5.443 21.554,5.421 C21.611,5.392 21.675,5.404 21.736,5.387 C21.770,5.377 21.798,5.355 21.834,5.349 C21.872,5.342 21.905,5.329 21.943,5.327 C21.963,5.326 21.980,5.315 22.000,5.315 C22.019,5.315 22.033,5.327 22.051,5.328 C22.106,5.330 22.153,5.353 22.207,5.365 C22.279,5.383 22.357,5.385 22.423,5.417 C22.430,5.420 22.439,5.417 22.446,5.421 C22.487,5.441 22.518,5.473 22.555,5.499 C22.569,5.509 22.588,5.507 22.602,5.518 C25.816,7.944 31.288,11.003 39.102,11.803 C39.612,11.855 40.000,12.285 40.000,12.797 L40.000,26.999 L40.000,27.112 C40.000,29.533 39.561,31.910 38.751,34.148 C38.744,34.169 38.740,34.189 38.731,34.209 C38.223,35.603 37.571,36.943 36.787,38.203 C36.762,38.269 36.717,38.325 36.678,38.386 C35.882,39.636 34.967,40.812 33.920,41.874 ZM35.629,36.214 C36.013,35.516 36.343,34.791 36.639,34.052 L30.586,27.999 L27.414,27.999 L35.629,36.214 ZM23.000,47.951 L25.008,46.421 L23.000,44.413 L23.000,47.951 ZM23.000,41.585 L26.613,45.198 L28.413,43.826 L23.000,38.413 L23.000,41.585 ZM23.000,35.585 L30.018,42.603 L30.515,42.225 C30.949,41.894 31.353,41.531 31.754,41.166 L23.000,32.413 L23.000,35.585 ZM23.000,29.585 L33.158,39.743 C33.660,39.178 34.114,38.575 34.544,37.958 L24.586,27.999 L23.000,27.999 L23.000,29.585 ZM21.000,19.585 L21.000,16.413 L11.413,25.999 L14.585,25.999 L21.000,19.585 ZM17.414,25.999 L21.000,25.999 L21.000,22.413 L17.414,25.999 ZM13.485,42.225 L21.000,47.951 L21.000,29.999 C21.000,29.999 21.000,29.999 21.000,29.999 L21.000,27.999 L6.021,27.999 C6.286,33.567 9.034,38.834 13.485,42.225 ZM6.000,25.999 L8.585,25.999 L21.000,13.585 L21.000,10.413 L6.000,25.413 L6.000,25.999 ZM6.000,13.690 L6.000,16.585 L9.517,13.067 C8.399,13.318 7.233,13.535 6.000,13.690 ZM13.461,11.952 L6.000,19.413 L6.000,22.585 L19.299,9.286 C17.648,10.239 15.703,11.170 13.461,11.952 ZM38.000,13.690 C31.353,12.853 26.367,10.470 23.000,8.245 L23.000,25.999 L25.000,25.999 C25.000,25.999 25.000,25.999 25.000,25.999 L31.000,25.999 C31.000,25.999 31.000,25.999 31.000,25.999 L38.000,25.999 L38.000,13.690 ZM37.343,31.928 C37.688,30.651 37.915,29.337 37.978,27.999 L33.414,27.999 L37.343,31.928 Z"/>
									</g>
								</svg>
							</div>
							<div class="title">
								Program 5
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
						</div>
					</div>
					<div class="col-md-4 col-6">
						<div class="program">
							<div class="icon">
								<svg width="54px" height="54px">
									<defs>
										<filter id="Filter_0">
											<feFlood flood-color="rgb(143, 176, 62)" flood-opacity="1" result="floodOut" />
											<feComposite operator="atop" in="floodOut" in2="SourceGraphic" result="compOut" />
											<feBlend mode="normal" in="compOut" in2="SourceGraphic" />
										</filter>
									</defs>
									<g filter="url(#Filter_0)">
										<path fill-rule="evenodd"  fill="rgb(30, 205, 226)" d="M53.000,26.999 C52.448,26.999 52.000,26.552 52.000,25.999 C52.000,12.766 41.233,1.999 28.000,1.999 C27.448,1.999 27.000,1.551 27.000,0.999 C27.000,0.447 27.448,-0.001 28.000,-0.001 C42.336,-0.001 54.000,11.662 54.000,25.999 C54.000,26.552 53.552,26.999 53.000,26.999 ZM28.000,5.999 C39.028,5.999 48.000,14.971 48.000,25.999 C48.000,26.552 47.552,26.999 47.000,26.999 C46.448,26.999 46.000,26.552 46.000,25.999 C46.000,16.073 37.925,7.999 28.000,7.999 C27.448,7.999 27.000,7.552 27.000,6.999 C27.000,6.447 27.448,5.999 28.000,5.999 ZM28.000,17.999 C32.411,17.999 36.000,21.588 36.000,25.999 C36.000,26.552 35.552,26.999 35.000,26.999 C34.448,26.999 34.000,26.552 34.000,25.999 C34.000,22.690 31.309,19.999 28.000,19.999 C27.448,19.999 27.000,19.551 27.000,18.999 C27.000,18.446 27.448,17.999 28.000,17.999 ZM28.000,11.999 C35.720,11.999 42.000,18.279 42.000,25.999 C42.000,26.552 41.552,26.999 41.000,26.999 C40.448,26.999 40.000,26.552 40.000,25.999 C40.000,19.382 34.617,13.999 28.000,13.999 C27.448,13.999 27.000,13.551 27.000,12.999 C27.000,12.447 27.448,11.999 28.000,11.999 ZM17.545,20.329 C16.600,21.525 16.700,23.240 17.778,24.318 L29.733,36.273 C30.783,37.323 32.553,37.428 33.722,36.507 L35.207,35.332 C36.957,33.951 39.626,33.941 41.385,35.309 L48.069,40.512 C49.201,41.393 49.901,42.719 49.990,44.151 C50.079,45.583 49.548,46.987 48.534,48.001 L44.265,52.270 C43.500,53.035 42.067,53.947 39.585,53.947 L39.584,53.947 C35.397,53.946 27.777,51.318 15.255,38.795 C7.812,31.353 3.027,24.892 1.034,19.594 C-1.122,13.866 0.576,10.991 1.780,9.787 L6.050,5.517 C6.995,4.572 8.251,4.051 9.587,4.051 C11.143,4.051 12.584,4.755 13.539,5.982 L18.741,12.665 C20.161,14.490 20.151,17.030 18.719,18.843 L17.545,20.329 ZM17.163,13.894 L11.961,7.210 C11.387,6.474 10.522,6.050 9.587,6.050 C8.786,6.050 8.032,6.364 7.464,6.931 L3.194,11.200 C1.704,12.691 1.601,15.422 2.906,18.890 C4.798,23.919 9.429,30.141 16.669,37.381 C29.342,50.054 36.466,51.947 39.584,51.947 L39.585,51.947 C41.382,51.947 42.353,51.354 42.850,50.857 L47.120,46.587 C47.729,45.978 48.047,45.135 47.994,44.275 C47.940,43.415 47.520,42.619 46.840,42.089 L40.157,36.888 C39.092,36.059 37.505,36.065 36.448,36.901 L34.961,38.076 C34.081,38.770 32.980,39.154 31.859,39.154 C30.521,39.154 29.264,38.633 28.319,37.687 L16.364,25.732 C14.568,23.937 14.401,21.082 15.976,19.089 L17.149,17.604 C18.010,16.514 18.015,14.989 17.163,13.894 Z"/>
									</g>
								</svg>
							</div>
							<div class="title">
								Program 6
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh</p>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="aws-details">
			<div class="container">
				<div class="flex-wrapper">
					<div>
						<p>If you’re looking for AWS experts – you’ve found them! With 14 AWS competencies and 1600+ certifications, KuwaitNet is a dedicated AWS business. Whether you’re looking to leverage the cloud for innovation, agility, cost savings, operational efficiency, or all of the above, our dedicated AWS team focuses on bringing the most cutting-edge AWS capabilities to our customers, all backed by our Fanatical Experience™.  With deep expertise and capabilities in cloud strategy, cloud-native development. modernization.all backed by our Fanatical Experience™.  With deep expertise and capabilities in cloud strategy, cloud-native development. modernization.</p>
					</div>
					<div class="image">
						<img src="src/images/aws-main.png" alt="aws partner consult">
					</div>
				</div>
				<div class="detail-wrapper justify-content-between">
					
					<div class="detail">
						<ul>
							<li>Machine Learning Competency</li>
							<li>Data & Analytics Competency</li> 
							<li>Premier Consulting Partner</li>
							<li>Managed Service Provider</li>
							<li>DevOps Competency​</li>
							<li>IoT Comptency</li>
						</ul>
					</div>
					<div class="detail">
						<ul>
							<li>Microsoft Workloads Competency</li><li>Education Competency</li>
							<li>Migration Competency</li>
							<li>Storage Competency</li>
							<li>Oracle Competency</li>
							<li>SaaS Competency</li>
						</ul>
					</div>
					<div class="detail">
						<ul>
							<li>Industrial Software Competency</li>
							<li>Financial Services Competency</li>
							<li>AWS Public Sector Partner</li>
							<li>Healthcare Competency</li>
							<li>Retail Competency</li>
							<li>AWS Reseller</li>
						</ul>
					</div>
					
				</div>
			</div>

		</div>

		<div class="aws-comparison">
			<div class="container">
				<div class="flex-wrapper">
					<div class="left-side">
						<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
						<h5>The 1st Generation to Grow Up With the Internet</h5>
						<p>lorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elit lorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum </p>
						<div class="button-row text-left">
							<a href="#" class="btn btn-default">Learn More</a>
						</div>
					</div>
					<div class="right-side">
						<div>
							<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
							<p>lorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elit</p>
							<a href="#" class="learn-more">Learn More</a>
						</div>
						<div class="has-margin">
							<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
							<p>lorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elitlorem ipsum dolor sit amet consectetuer adipiscing elit</p>
							<a href="#" class="learn-more">Learn More</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="contact-sec">
			<div class="container">

				<div class="section-title">
					Contact Us
				</div>

				<p class="section-subtitle">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut urna vitae nunc lobortis mollis. Pellentesque at fringilla erat. Morbi in lectus posuere, congue justo vitae, malesuada nibh
				</p>

				<form>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">First Name</label>
								<input type="text" class="form-control" value="John"> 
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Last Name</label>
								<input type="text" class="form-control" placeholder="Doe"> 
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">E-mail</label>
								<input type="email" class="form-control" placeholder="name@mail.com"> 
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Your Phone</label>
								<input type="tel" class="form-control" placeholder="+965 2244 8434 "> 
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">Your Message</label>
								<textarea class="form-control" placeholder="Write a few words about your business"></textarea>
							</div>
						</div>
					</div>
					<div class="button-row">
						<button class="btn btn-primary lg">Submit</a>
					</div>
				</form>

			</div>
		</div>

    </div>
<?php include_once "includes/footer.php";?>