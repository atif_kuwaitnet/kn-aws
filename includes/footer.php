	
	<footer class="footer">  
		<div class="container">
			
			
			<div class="foot-flex">
				<p>© 2021 All Rights Reserved.</p>
				<ul class="nav social-nav"> 
					<li><a href="https://www.youtube.com/user/KuwaitNET" target="_blank" class="fb"><i class="fab fa-youtube"></i></a></li>
					<li><a href="https://www.facebook.com/KUWAITNET/" target="_blank" class="fb"><i class="fab fa-facebook"></i></a></li>
					<li><a href="https://www.instagram.com/kuwaitnet/"><i class="fab fa-instagram"></i></a></li>
					<li><a href="https://twitter.com/KuwaitNET"><i class="fab fa-twitter"></i></a></li>
					<li><a href="https://www.linkedin.com/company/kuwaitnet/"><i class="fab fa-linkedin"></i></a></li>
				</ul>
			</div>
			
		</div>
	</footer>
	
</div>

</body>
</html>