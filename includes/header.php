<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Kn2021</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.0-alpha14/css/tempusdominus-bootstrap-4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>
    <link href="src/css/jquery.mCustomScrollbar.css" rel="stylesheet">
    <link href="src/css/bootstrap-select.css" rel="stylesheet">
    <link href="src/css/slick.css" rel="stylesheet">
    <link href="src/css/main.css" type="text/css" rel="stylesheet">  

    <script src="https://code.jquery.com/jquery-3.4.0.min.js" integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="src/js/moment.min.js"></script>
    <script src="src/js/jquery.mCustomScrollbar.js"></script>
    <script src="src/js/jquery.mousewheel.min.js"></script>
    <script src="src/js/bootstrap-select.js"></script>
    <script src="src/js/slick.js"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script src="src/js/custom.js"></script>
    
</head>

<body <?php if(isset($ishome)){ echo "class='front-page'";}?> >
    <!-- <div class="search-overlay"></div> -->
    <!-- <div class="modal custom-modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog ">
            <div class="modal-content">
                <span class="popup-cross-icon" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-times"></i>
                </span>
                <div class="modal-body">
                    Modal contents
                </div>
            </div>
        </div>
    </div> -->
    <!-- <div class="modal fade small login" id="signin" tabindex="-2" role="dialog" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <span class="popup-cross-icon" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-times"></i>
                </span>
                <div class="modal-body">
                    <div class="login-signup">
                        <div class="inner-spacing">
                            <h2 class="head">Log In </h2>
                            <form>
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input type="text" name="required" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Password</label>
                                    <input type="password" name="required" class="form-control" placeholder="">
                                    <span class="forgot"><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#forgot-pass" >Forgot?</a></span>
                                    
                                </div>
                                <div class="button-row m-0">
                                    <button type="button" class="btn btn-default">Log In</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- add new review modal -->
    <div class="modal custom-modal fade medium rate-item" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <span class="popup-cross-icon" data-dismiss="modal" aria-hidden="true">
                    <i class="fa fa-times"></i>
                </span>
                <div class="modal-body">
                    
                </div>
            </div>
        </div>
    </div>
    <!-- add new review modal -->

    
    <div class="wrapper mp-pusher" id="mp-pusher">
        
        <div class="header-fixed">
            <header>
                <div class="container">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-5">
                            <a href="#" class="logo">
                                <img src="src/images/logo-white.png" alt="header logo" title="header logo">
                            </a>
                        </div>
                        <div class="col-7">
                            <ul class="d-flex align-items-center">
                                <li class="search">
                                    <a href="#" class="icon">
                                        <i class="fas fa-search"></i>
                                    </a>
                                    <!-- <a href="#" class=" close-icon">
                                        <i class="fa fa-times"></i>
                                    </a> -->
                                </li>
                                <li class="menu-icon menu-trigger">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        </div>
            



        
     
        

